function msgToSelf(){

	console.log("Don't text her back")
}

// WHILE LOOP
let count = 10

while(count !== 0){
	msgToSelf();

count --;
}

// Display from  1 - 5 (in order)

let num = 1

while(num <= 5){
	console.log(num)
	num ++;
}

// --------------------------------

/*
	DO-WHILE LOOP

		is similar to the while loop, however, do while will allow us to run loop at least once.

	WHILE
		we check the condiion first before running our codeblock

	DO-WHILE
		it will do an instruction first before iit will check theh condition to run again	
	Create a do-while loop 
*/

let counter = 1;

do{
	console.log(counter)
	counter++;
}while(counter <= 20)

// ----------------------------------

/*
	FOR LOOP

		Initialization , condition, final

*/

// for(let count = 0; count <=10; counter ++){
// 	console.log(count)
// }


let fruit = ["Apple", "Mango", "Pineapple", "Guyabano"]

for(count = 0; count < fruit.length; count ++){
	console.log(fruit[count]);
}

// -----------

let favoriteCountries = ["Philippines", "Japan", "U.K", "Korea"]

for(let count = 0; count < favoriteCountries.length; count++){
	console.log(favoriteCountries[count])
}


// ---------------------------------------------

/*
	GLOBAL/LOCAL SCOPE

	GLOBAL Scope Variable
		can be accessed inside a function or anywhere else in our script

*/

let userName = 'super_man_21';

function sample(){
	// local scope or function scoped variable
	// cannot be accessed outside the function it was created from
	let heroName = "One Punch Man"
	console.log(heroName);
	console.log(userName);
}

sample()

console.log(userName)
/*console.log(heroName)*/


// -----------------------------------------------

/*
	Strings are similar to arrays
*/

let powerpuffGitls = ['Blossom', 'Bubbles', 'Buttercup'];

console.log(powerpuffGitls[0]);

let name = "Alexandro";

console.log(name[0])
console.log(name[name.length-1])



/*
	create a function to able to recieve a string as argument

		-Inside the function add a for loop

			-this for loop should be able to show/display all character of word/string.
			-note: string are similar to arrays
*/
function test(string) {
	for (let x = 0; x < string.length; x++) {
		console.log(string[x])
	}
}

test("meow")

