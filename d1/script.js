/*
	JSON OBJECTS

		JSON stands for javaScript Object Notation

		JSON is used for serializing different data tyoe into bytes.

		Serialization is the process of converting data into a series of bytes for easier transmission/ transfer of information.

		byte - binary digits (1 and 0) that is used to represent a character
			
		JSON data format
		SYNTAX:

		{
			"propertyA": "valueA"
			"propertyB": "valueB"
		}

		JSON
		{
			"city": "QC",
			"province": "Metro Manila",
			"country": "Philippines"

		}

		JSON ARRAY

		"cities": [
			{
				"city": "QC",
				"province": "Metro Manila",
				"country": "Philippines"
			},
			{
				"city": "QC",
				"province": "Metro Manila",
				"country": "Philippines"
			},
			{
				"city": "QC",
				"province": "Metro Manila",
				"country": "Philippines"
			}
		]
				

		JS OBJECT
		{
			city: "QC",
			province: "Metro Manila",
			country: "Philippines"
		}
	
*/


/*
	JSON Methods

		The JSON contains methods for parsing and converting data into a stringified JSON

*/

// ----------------------------------------------------------------

/*

	FROM CLIENT TO SERVER



	Stringified JSON 

		is a javascript object converted into a string to be used in other function of a Javascript application
*/


let batchesArr = [
	{
		batchName: 'Batch X'
	},
	{
		batchName: 'Batch Y'
	}
]

console.log('Result from strinngified method')
// The 'Stringify' method is used to convert JS object into JSON(string) 
console.log(JSON.stringify(batchesArr))



let data = JSON.stringify({
	name: 'John',
	age: 31,
	address:{
		city: 'Manila',
		country: 'Philippines'
	}
})

console.log(data)




/*
	MINI ACTIVITY

		Use the prompt method in order to gather user data to be supplied to the user datails, then once the data gathered, convert it into stringify and print the details

		Details:

		firstname:
		lastName:
		age:
		address: {
			city:
			country:
			zip code:
		}

*/
// MINI ACTIVITY
// let firstName = prompt("First Name: ")
// let lastName = prompt("Last Name: ")
// let age = prompt("Age: ")
// let address = [prompt("City: "), prompt("Country: "), prompt("Zip Code: ")]


// let userDetails = JSON.stringify({
// 	firstName: firstName,
// 	lastName: lastName,
// 	age: age,
// 	address: address
// })

// console.log(userDetails)

// -------------------------------------------------------------

/*
	SERVER TO CLIENT


	Coverting Stringified JSON into JS objects.

	Information is commonly sent to application in stringified JSON and then converted back into objects.

	This happens both for sending information to a backend app and sending information back to frontend app.

	Upon receving
*/

let batchesJSON = `[
	{
		"batchName": "Batch X"
	},
	{
		"batchName": "Batch Y"
	}
]`

console.log(`Result from parse method:`)
console.log(JSON.parse(batchesJSON))


let stringifiedObject = `
	{
		"name": "John",
		"age": "'31",
		"address": {
			"city": "Manila",
			"country": "Philippines"
		}
	}

`
console.log(JSON.parse(stringifiedObject))
